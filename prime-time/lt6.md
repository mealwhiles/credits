# Have You Ever Wanted Movies Free?  
LT6 Have You Ever Wanted Movies Free?  
https://youtube.com/watch?v=7aZ1thqlS2E

Assets that are not credited are made by me.  
If you find a mistake, please contact me!

## Contact
mealwhiles@mealwhiles.com

PGP Public Key  
https://keyserver.pgp.com/vkd/DownloadKey.event?keyid=0x6B7126B9690CF3A5  
E98F31A59E315126C46E3A6C6B7126B9690CF3A5  
Tutanota emails will be encrypted properly.

## Graphics
[0] Thumbnail  
"Bald Mario"  
by D1119  
https://reddit.com/r/casualnintendo/comments/8m1u2g

[1] Start  
"Inside Joke Drawings"  
by PortalBot

## Music

{0} Start  
"Jump Up, Super Star! Karaoke JP"  
by Naoto Kubo  
of Nintendo  
for Super Mario Odyssey    
https://nintendo.com

{1} End
"Super Mario Song Cringe Lofi Remix"  
by pan-dough

## Other

<0> Start  
"Liberation Sans"  
typeface asset  
by Steve Matteson  
licensed by Red Hat

<1> Start  
"Gotham Rounded"  
typeface asset

## Freedom-respecting Software

"Open Broadcaster Software"  
for recording  
https://obsproject.com

"GNU Image Manipulation Program"  
for image manipulation  
https://gimp.org

## Proprietary Software

"Windows"  
for operating system  
https://windows.com

"Super Mario Odyssey"  
for gameplay
https://supermario.nintendo.com

Elgato Game Capture HD  
for capture card drivers  
https://elgato.com
