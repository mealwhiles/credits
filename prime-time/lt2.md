# Dreams™ but Wet™  
LT2 Dreams™ but Wet™  
https://youtube.com/watch?v=7EQyFUrdaEo

Assets that are not credited are made by me.  
If you find a mistake, please contact me!

## Contact
mealwhiles@mealwhiles.com

PGP Public Key  
https://keyserver.pgp.com/vkd/DownloadKey.event?keyid=0x6B7126B9690CF3A5  
E98F31A59E315126C46E3A6C6B7126B9690CF3A5  
Tutanota emails will be encrypted properly.

## Graphics

[0] Start  
"Inside Joke Drawings"  
by PortalBot

## Music

{0} Start  
"The Dreamiverse"  
by Jim Fowler & Joe Thwaites  
of Media Molecule  
for Dreams (PS4)  
https://mediamolecule.com

{1} End
"Super Mario Song Cringe Lofi Remix"  
by pan-dough

## Other

<0> Start  
"Liberation Sans"  
typeface asset  
by Steve Matteson  
licensed by Red Hat

<1> Start  
"Gotham Rounded"  
typeface asset

## Freedom-respecting Software

"Open Broadcaster Software"  
for recording  
https://obsproject.com

"GNU Image Manipulation Program"  
for image manipulation  
https://gimp.org

## Proprietary Software

"Windows"  
for operating system  
https://windows.com

"Dreams"
for gameplay
https://dreams.mediamolecule.com

Elgato Game Capture HD  
for capture card drivers  
https://elgato.com
