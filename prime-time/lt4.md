# Mario's a Woke Influencer!
LT4 Mario's a Woke Influencer!  
https://youtube.com/watch?v=ZPlBCTpapzA

Assets that are not credited are made by me.  
If you find a mistake, please contact me!

## Contact
mealwhiles@mealwhiles.com

PGP Public Key  
https://keyserver.pgp.com/vkd/DownloadKey.event?keyid=0x6B7126B9690CF3A5  
E98F31A59E315126C46E3A6C6B7126B9690CF3A5  
Tutanota emails will be encrypted properly.

## Graphics

[0] Start  
"Inside Joke Drawings"  
by PortalBot

## Music

{0} Start  
"New Donk City"  
by Qumu Music  
remixed Super Mario Odyssey  
https://qumumusic.com

{1} End
"Super Mario Song Cringe Lofi Remix"  
by pan-dough

## Other

<0> Start  
"Liberation Sans"  
typeface asset  
by Steve Matteson  
licensed by Red Hat

<1> Start  
"Gotham Rounded"  
typeface asset

## Freedom-respecting Software

"Open Broadcaster Software"  
for recording  
https://obsproject.com

"GNU Image Manipulation Program"  
for image manipulation  
https://gimp.org

## Proprietary Software

"Windows"  
for operating system  
https://windows.com

"Super Mario Odyssey"  
for gameplay
https://supermario.nintendo.com

Elgato Game Capture HD  
for capture card drivers  
https://elgato.com
